const ComplexSearchSettings_Gaming = {
  type: 'complexSearch',
  blacklist: ['buy one', 'bogo', 'free shipping'],
  whitelist: ['xbox\\wlive', 'game\\w?pass', 'wireless.*mouse', 'headset', 'controller', 'free']
};

module.exports = ComplexSearchSettings_Gaming;
