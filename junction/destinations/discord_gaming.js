const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  extraOptions: {
    url: 'https://discord.com/api/webhooks/912566198401642507/H80Osv6C4eeVk_addTurTMywzwK192-yAFpT9cgAlKEfUPkjP1__Wwqs-t-wvGpOTzs6'
  }
});

module.exports = config;
