const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  extraOptions: {
    url: 'https://discord.com/api/webhooks/912187330733948968/n3FM_DMYDnOsjQJDj0lQCAMi4pg-4kYaDvPTuMeXkzYnjtUwlM8nkImdVQlKbuqB_FBB'
  }
});

module.exports = config;
