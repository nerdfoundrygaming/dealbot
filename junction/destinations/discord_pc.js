const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  extraOptions: {
    url: 'https://discord.com/api/webhooks/907756988740554833/DrufzQ61uMyfXFvgnIkg5uvsnPahfd0UojJVUyxwLFBtFLXf2K2-01lLwnnBAuf6hFCP'
  }
});

module.exports = config;
