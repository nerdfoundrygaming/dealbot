const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  extraOptions: {
    url: 'https://discord.com/api/webhooks/912597302378635284/dXLvGjMgSTlwoqeXB7oDr1altPTdqK6GoUChqtF9pzv9ar15JQ1ycxp_ZeJwK2eTALz0'
  }
});

module.exports = config;
