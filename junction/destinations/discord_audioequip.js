const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  template: (sourceLoader, entry) => ({
    content: 'Listen to this Deal!',
    embeds: [
      {
        color: 3447787,
        description: `${sourceLoader.getEntryName(entry)}\n\n**[>> View Deal <<](${sourceLoader.getEntryUrl(
          entry
        )})**\n\nReview Hardware Before Purchasing!`
      }
    ]
  }),

  extraOptions: {
    url: 'https://discord.com/api/webhooks/911477537631264828/nT22n4tTYwFecrpjkMQ5h7_-otWXgT68bFihlWYN0YplZWa2belBuUQ3R2Rce8NM46ok'
  }
});

module.exports = config;
