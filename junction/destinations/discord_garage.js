const merge = require('lodash.merge');

const baseDiscord = require('../descriptorTemplates/base_destination_discord');

const config = merge({}, baseDiscord, {
  extraOptions: {
    url: 'https://discord.com/api/webhooks/910086000347320320/tRybMfR5lzxjDHcNHkD0e82vl1qXAcRkgYNJycEMT2n_sb9-hNf-WXBX5laweicy15af'
  }
});

module.exports = config;
