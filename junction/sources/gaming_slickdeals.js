const merge = require('lodash.merge');

const baseSlickDeals = require('../descriptorTemplates/base_source_slickdeals');
const ComplexSearchSettings_Gaming = require('../searchTerms/gaming');

const config = merge({}, baseSlickDeals, {
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_gaming',
  predicate: ComplexSearchSettings_Gaming
});

module.exports = config;
