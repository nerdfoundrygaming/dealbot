const ComplexSearchSettings_Gaming = require('../searchTerms/gaming');

const config = {
  type: 'rss',
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_gaming',
  extraOptions: {
    feed: 'https://www.reddit.com/r/GameDeals/.rss?sort=new&limit=500'
  },
  predicate: ComplexSearchSettings_Gaming
};

module.exports = config;
