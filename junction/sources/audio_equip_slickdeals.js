const merge = require('lodash.merge');

const baseSlickDeals = require('../descriptorTemplates/base_source_slickdeals');

const config = merge({}, baseSlickDeals, {
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_audioequip',
  predicate: {
    type: 'complexSearch',
    blacklist: ['mouse'],
    whitelist: ['receiver', 'speakers']
  }
});

module.exports = config;
