const merge = require('lodash.merge');

const baseSlickDeals = require('../descriptorTemplates/base_source_slickdeals');

const config = merge({}, baseSlickDeals, {
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_garage',
  predicate: ['shopvac', 'shop vac', 'drill', 'impact', 'Dash Cam', 'chainsaw']
});

module.exports = config;
