const merge = require('lodash.merge');

const baseSlickDeals = require('../descriptorTemplates/base_source_slickdeals');
const ComplexSearchSettings_PC = require('../searchTerms/pc_gear');

const config = merge({}, baseSlickDeals, {
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_pc',
  predicate: ComplexSearchSettings_PC
});

module.exports = config;
