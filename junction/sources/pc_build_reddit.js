const ComplexSearchSettings_PC = require('../searchTerms/pc_gear');

const config = {
  type: 'rss',
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_pc',
  extraOptions: {
    feed: 'https://www.reddit.com/r/buildapcsales/.rss?sort=new&limit=500'
  },
  predicate: ComplexSearchSettings_PC
};

module.exports = config;
