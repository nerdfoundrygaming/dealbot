const merge = require('lodash.merge');

const baseSlickDeals = require('../descriptorTemplates/base_source_slickdeals');

const config = merge({}, baseSlickDeals, {
  destinations: process.env.NODE_ENV === 'development' ? 'discord_testing' : 'discord_kitchen',
  predicate: {
    type: 'complexSearch',
    blacklist: ['expansion'],
    whitelist: ['pots', 'pans', 'deyhydrator']
  }
});

module.exports = config;
