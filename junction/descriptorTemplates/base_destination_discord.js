const config = {
  type: 'http_post',
  template: (sourceLoader, entry) => ({
    content: 'Deal Item Found!',
    embeds: [
      {
        color: 3447787,
        description: `${sourceLoader.getEntryName(entry)}\n\n**[>> View Deal <<](${sourceLoader.getEntryUrl(
          entry
        )})**\n\nReview Hardware Before Purchasing!`
      }
    ]
  }),
  extraOptions: {
    url: 'NEED TO SUPPLY',
    requestLimits: {
      tokensPerInterval: 3,
      interval: 1000
    }
  }
};

module.exports = config;
