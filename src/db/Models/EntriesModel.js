const Logger = require('-/Logger');
const { bs } = require('..');

const model = bs.model('Entries', {
  tableName: 'entries',
  hasTimestamps: true
});

/**************************************************************************************************************************************
 * Import/Notify Methods
 *************************************************************************************************************************************/

model.getEntriesForDestinationId = destinationId =>
  this.where('destinationIds', 'like', `%${destinationId}%`).where('isNotified', false).fetchAll();

model.markNotified = entry => model.forge({ id: entry.id }).save({ isNotified: true, error: null });
model.markErrored = (entry, err) => model.forge({ id: entry.id }).save({ isNotified: false, error: err.message });

/**************************************************************************************************************************************
 * Prune Methods
 *************************************************************************************************************************************/

model.pruneKeepMaximum = async (sourceId, maximumAmount, isDeleted) => {
  // prettier-ignore
  //not deleted && sourceId matches && isNotified
  const clauseValidMatchingSourceId = () =>
    this
      .where('sourceId', sourceId)
      .where('isDeleted', isDeleted)
      .where('isNotified', true);

  const numTotalValidEntries = await clauseValidMatchingSourceId().count();

  if (numTotalValidEntries <= maximumAmount) {
    return {
      sourceId,
      numTotalValidEntries: 0
    };
  }

  // prettier-ignore
  const keepMaximumModels = await clauseValidMatchingSourceId()
    .orderBy('-created_at')
    .fetchPage({
      pageSize: maximumAmount
    });

  const keepMaximumIds = keepMaximumModels.serialize().map(source => source.id);

  const numDeleted = Math.max(0, numTotalValidEntries - maximumAmount);

  // prettier-ignore
  // Delete items that are not to be kept around
  await this
    .where('sourceId', sourceId)
    .where('id', 'not in', keepMaximumIds)
    .destroy({ require: false });

  return {
    sourceId,
    numTotalValidEntries,
    maximumAmount,
    numDeleted
  };
};

const _pruneOrphaned = async clauseThunk => {
  const numOrphaned = await clauseThunk().count();

  if (!numOrphaned) {
    return 0;
  }

  await clauseThunk().destroy({ require: false });

  return numOrphaned;
};

model.pruneOrphanedSources = async knownIds => {
  return _pruneOrphaned(() => this.where('sourceId', 'not in', knownIds));
};

model.pruneOrphanedDestinations = async knownIds => {
  const clauseOrphaned = () =>
    knownIds.reduce((_this, destId) => {
      return _this.where('destinationIds', 'not like', `%${destId}%`);
    }, this);

  return _pruneOrphaned(clauseOrphaned);
};

model.pruneErroredEntries = async (numDays, softDelete) => {
  const daysAsMilliseconds = numDays * (24 * 60 * 60) * 1000;

  const daysAgoAsMilliseconds = new Date(Date.now() - daysAsMilliseconds).getTime();

  // error <> null updated_at > numDaysTTL
  const clauseOldErroredEntries = () =>
    this.where('error', '<>', '').where('isDeleted', false).where('updated_at', '<=', daysAgoAsMilliseconds);

  const numFoundEntires = (await clauseOldErroredEntries().fetchAll()).serialize().length;

  if (softDelete) {
    await clauseOldErroredEntries().save({ isDeleted: true }, { method: 'update', patch: true, require: false });
  } else {
    await clauseOldErroredEntries().destroy({ require: false });
  }

  return numFoundEntires;
};

module.exports = model;
