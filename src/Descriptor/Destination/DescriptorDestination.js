const Mustache = require('mustache');

const EntriesModel = require('-/db/Models/EntriesModel');
const Logger = require('-/Logger');
const { FinalizeAndNormalize } = require('-/Util');

module.exports = class DescriptorDestination {
  config = null;
  serializedData = null;
  notifyData = null;

  get id() {
    return this.config?.id;
  }

  constructor(config) {
    this.config = config;
  }

  async hydrate() {
    const collection = await EntriesModel.getEntriesForDestinationId(this.config.id);

    this.serializedData = collection.serialize();
  }

  async getTemplatedEntry(sourceLoader, entry) {
    if (!this.config.template) {
      throw new Error(`[${sourceLoader}] Missing Template Definition`);
    }

    const isFunction = typeof this.config.template === 'function';
    const isString = typeof this.config.template === 'string';

    if (isString) {
      return Mustache.render(this.config.template, {
        // prettier-ignore
        title: sourceLoader.getEntryName(entry),
        url: sourceLoader.getEntryUrl(entry)
      });
    }

    if (isFunction) {
      const retVal = this.config.template(sourceLoader, entry);

      let isString = typeof retVal === 'string';

      return isString ? retVal : JSON.stringify(retVal);
    }

    throw new Error(`[${sourceLoader}] Unknown Template Type: ${typeof this.config.template}`);
  }

  async deserializeEntries(sourceLoadMap) {
    let sourceId, chosenSourceLoader;

    const deserializedModels = this.serializedData.map(async entry => {
      sourceId = entry.sourceId;
      chosenSourceLoader = sourceLoadMap[sourceId];

      if (!chosenSourceLoader) {
        Logger.error(`Unable to parse saved Entry [${entry.idHash}], invalid Source ID: ${sourceId}`);

        return null;
      }

      const deserializedData = await chosenSourceLoader.getEntryDeserializedData(entry.content);

      // NotifyData type
      return {
        entry,
        deserializedData,
        sourceLoader: chosenSourceLoader,
        templatedEntry: await this.getTemplatedEntry(sourceLoadMap[entry.sourceId], deserializedData)
      };
    });

    const notifyData = await FinalizeAndNormalize(deserializedModels);

    this.notifyData = notifyData;
  }

  async iterateNotifies(cb) {
    // prettier-ignore
    return this
      .notifyData
      .reduce(async (promiseChain, notifyData) => await promiseChain.then(cb.bind(cb, notifyData)), Promise.resolve())
  }

  async notifyDestination(notifyData) {
    throw new Error('Not implemented in DescriptorDestination');
  }
};
