const fetch = require('node-fetch');

const { RateLimiter } = require('limiter');

const Logger = require('-/Logger');
const DescriptorDestination = require('./DescriptorDestination');

module.exports = class HttpPostDestination extends DescriptorDestination {
  get url() {
    return this.config?.extraOptions.url;
  }

  constructor(config) {
    super(config);

    if (!config.extraOptions.requestLimits) {
      throw new Error(`[${config.id}] ` + 'HTTP Post Descriptor must have `extraOptions.requestLimits` defined!');
    }

    this._limiter = new RateLimiter(config.extraOptions.requestLimits);
  }

  async notifyDestination({ deserializedData, sourceLoader, templatedEntry }) {
    try {
      await this._limiter.removeTokens(1);

      // Then try to Attempt Notify...
      const resp = await this._httpPost(templatedEntry);

      if (!resp.ok) {
        const err = await resp.json();
        throw new Error(err.message);
      }

      return resp;
    } catch (err) {
      throw err;
    }
  }

  _updateTokenBucket(rateLimit, rateWindow) {
    // Reset the request limits based on response headers
    this.config.extraOptions.requestLimits.tokensPerInterval = rateLimit;
    this.config.extraOptions.requestLimits.interval += rateWindow / 8;

    this._limiter.tokenBucket.bucketSize = rateLimit;
    this._limiter.tokenBucket.tokensPerInterval = rateLimit;
    this._limiter.tokenBucket.interval = this.config.extraOptions.requestLimits.interval;
  }

  _sleep = duration => new Promise(r => setTimeout(r, duration));

  async _fetchAndRetry(requestor) {
    const resp = await requestor();

    if (429 !== resp.status) {
      return resp;
    }

    //! TODO: Determine which methodology to use
    // const respResetEpoch = +resp.headers.get('x-ratelimit-reset');
    // const respRateLimit = +resp.headers.get('X-RateLimit-Limit');
    // const nowEpoch = Math.round(Date.now() / 1000);
    // const resetTime = respResetEpoch - nowEpoch + 1;
    // const respDuration = resetTime * 1000;

    const respRateLimit = +resp.headers.get('X-RateLimit-Limit');
    const respDuration = +resp.headers.get('retry-after');

    this._updateTokenBucket(respRateLimit, respDuration);

    Logger.debug(`Exceeded API Request Rate, will Retry [Retry-After]: ${respDuration}`);
    Logger.debug(
      `Setting Rate Limit to ${this.config.extraOptions.requestLimits.tokensPerInterval} and Rate Window to ${this.config.extraOptions.requestLimits.interval}`
    );

    // Wait for the time specified in the 429 response
    await this._sleep(respDuration);

    // Retry after we've slept (on the job)
    return this._fetchAndRetry(requestor);
  }

  async _httpPost(templatedEntry) {
    return await this._fetchAndRetry(async () => {
      return fetch(this.url, {
        method: 'POST',
        //! FIXME: This needs to move to overlay concept
        headers: { 'Content-Type': 'application/json' },
        body: templatedEntry
      });
    });
  }
};
