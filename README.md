# Deal Bot

- [Deal Bot](#deal-bot)
  - [Building](#building)
    - [Production Build](#production-build)
    - [Development Build](#development-build)
  - [Running](#running)
    - [Envionment Variables](#envionment-variables)
    - [Production Mode](#production-mode)
      - [Docker](#docker)
      - [Docker Compose](#docker-compose)
        - [Configuration](#configuration)
        - [Running](#running-1)
    - [Development Mode](#development-mode)
        - [Hosting the Development Environment](#hosting-the-development-environment)
        - [Iterating the Build](#iterating-the-build)
- [Road Map](#road-map)
    - [Nice to haves](#nice-to-haves)

## Building

// ! FIXME - May not be true anymore?? Test thoroughly

> When switching between build environments (aka prod->dev), it's important to first remove the previous layers to avoid invalid caching!
> This is most important when stepping *down* a build stage, but as habit you should prune these images to be safe!
>
> ```
> docker rmi -f nerdfoundry/dealbot:latest
> ```
>
> This should get rid of all layers pertaining to the current image, allowing a cache to be recreated.

### Production Build

```sh
docker-compose -f ./docker-compose.yml -f ./docker-compose.prod.yml build
```

### Development Build

```sh
docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml build
```

## Running

### Envionment Variables

| Env Var                                     | Default Value                                              | Info                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ------------------------------------------- | ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| LOG_LEVEL                                   | info                                                       | Set the output Log Level choices: `trace`, `debug`, `info`, `warn`, `silent`)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| PATH_DESCRIPTORS_BASE                       | "./junction"                                               | Path to directory for Source/Destination Descriptors base directory. Not to be used in a `docker-compose`<br />setting, you can simply volume mount instead.                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| DESCRIPTOR_EXTENSIONS                       | ".js .json"                                                | Extensions to parse in PATH_DESCRIPTORS_BASE (include full '.ext' and space separated)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| CRONTAB_PATTERN_IMPORT                      | [`*/5 * * * *`](https://crontab.guru/#*/5_*_*_*_*)         | Import Entries from Source Descriptors, to be batched later for Destination Descriptors.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| CRONTAB_PATTERN_NOTIFY                      | [`*/10 * * * *`](https://crontab.guru/#*/10_*_*_*_*)       | Process previously Imported Entries to be Notified to defined Destinations Descriptors.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| CRONTAB_PATTERN_REPORT_PERIODIC             | [`1 0 * * 1`](https://crontab.guru/#12_0_*_*_1)            | Report Periodic Stats, showing current trends for the current period. The current period is reset every time this is ran,<br />and will be used for the Comparison job!                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| CRONTAB_PATTERN_REPORT_COMPARE              | [`2 0 * * 1`](https://crontab.guru/#2_0_*_*_1)             | Report Comparison Stats, showing differentials between periods to see hotspots witin the year.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| CRONTAB_PATTERN_PRUNE_ERRORED_NOTIFICATIONS | [`0 0 * * 0,2,4,6`](https://crontab.guru/#0_0_*_*_0,2,4,6) | Prune *Errored* Notifications that haven't been sent after `PRUNE_ERRORED_TTL_DAYS` days.<br/>Actual deletion (i.e., *hard* vs *soft* deletes) depends on whether `PRUNE_SOFT_DELETE` holds *any* value.                                                                                                                                                                                                                                                                                                                                                                                                                              |
| CRONTAB_PATTERN_PRUNE_KEEP_MAXIMUM          | [`0 0 1 * *`](https://crontab.guru/#0_0_1_*_*)             | Prune Old (non-Errored, and successfully notified) Entries, per Source Descriptor, keeping only<br />`maxEntryRetainerCount` (see *Descriptor Config: Sources*) number of Entries. This can speed up performance by<br />deleting old and notified Entries, while not allowing upcoming Source Scans to be reimported (because of the overlap<br />from `maxEntryRetainerCount` and existing *Errored* Entries). This will also clean orphaned Entries that will ultimately<br />never be notified (usually due to a renamed or deleted Destination Descriptor). **All Deletions in this job are *hard*<br />deletes and permanent!** |
| CRONTAB_PATTERN_PRUNE_SOFT_DELETED          | [`0 0 1 * *`](https://crontab.guru/#0_0_1_*_*)             | Prune all the Old Soft Deleted Entries. These were once Errored entries that were kept around to avoid duplicate<br />Import/Notification for stale Sources. This is effectively identical to the *Prune Keep Maximum* job by keeping the<br />same `maxEntryRetainerCount` Entries, but targets and hard-deletes previously pruned Entries.                                                                                                                                                                                                                                                                                          |
| CRONTAB_PATTERN_PRUNE_REPORTS               | [`0 0 1 5 *`](https://crontab.guru/#0_0_1_5_*)             | Prune all reports to restart comparison values (as well as current period).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| PRUNE_ERRORED_TTL_DAYS                      | 7                                                          | Any *Errored* Notifications that have lived longer than this setting will be considered for Pruning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| PRUNE_SOFT_DELETE                           | *None*                                                     | When Pruning *Errored* Notifications, indicate whether to *actually* delete or not. Just needs to be set to *any* value to be true.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

> Use [Crontab Guru](https://crontab.guru) and [CronMaker](http://www.cronmaker.com/) to help generate Crontab Patterns if you need help!
> The Docker Image uses Alpine Linux, and supplies a `busybox` version of `crond`.

### Production Mode

> Production Mode is only supported via `docker`, as that's both the development environment and official deployment method.

#### Docker

```sh
VOLUME_PATH=$(pwd) docker run --rm --name dealbot \
    -v $VOLUME_PATH/dealbot/junction:/build/junction \
    -v $VOLUME_PATH/volumes/dealbot/db:/app/db \
    nerdfoundry/dealbot:latest
```

#### Docker Compose

##### Configuration

```yml
version: '3.7'

services:
  dealbot:
    image: nerdfoundry/dealbot:latest
    environment:
        # Import every 2min instead of default 5min
        - CRONTAB_PATTERN_IMPORT=*/2 * * * *
    volumes:
      - './volumes/dealbot/junction:/app/junction'
      - './volumes/dealbot/db:/app/db'
```

##### Running

```sh
docker-compose up
```

### Development Mode

To run in *Development Mode*, you'll need 2 shells: one to host the environment, and the other to iterate the build.

##### Hosting the Development Environment

```sh
docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml up
```

> When starting up, you may see `EACCES: permission denied, scandir '/root/.npm/_logs'` and can ignore it.

##### Iterating the Build

Technically you'll recieve instructions from the `compose` command, but for posterity sake, at this point you can now run the dev build cycle.

```sh
docker exec -it dealbot_dealbot_1 ash
```

# Road Map

* Pruning
    * Soft_Deleted items need clearing at some frequency (very large, like monthly)
        * needs env var
        * needs job
* Reporting
    * Needs an entire pass
        * Needs updating code to update stats accurately
            * Currently places assume success prior to work actually done (i.e., notifyDestinations)
    * Simple output
        * Consider per-Source/Destination for all stats
        * Number of errored items
            * How many of them are Soft Deleted
        * Stats?
          * Needs new migration/table
          * How many matches overall
          * How many blacklisted overall
          * How many notifies overall
          * How many notify errors overall
          * Maybe keep track of an object for weekly comparisons, and a running report style of last n-weeks?
* Cron Update
    * When 2 cronjobs run at the same time, it's impossible to tell which is which
        * Add the job type to the debugger label (currently just DealBot)
* Database and volume management needs to be figured out
    * sqlite db as well as remote configuration needs to be available
        * ie, ship with sqlite enabled, but allow knex configs

* Context pass:
    * Notifications really means Entries, update to Entries everywhere (but not blindly, take a look)
    * Consider refactoring JobManager so jobs are actually isolated
        * Keep importing descriptors to Job Manager, but actual job work move out
* Doc Add
    * Template encoding for title, but not url
    * Document example descriptors FULLY and ANNOYINGLY!
    * Document here about the descriptor file structure and their intent (source vs dest, multi srcs can go to single dest)
    * Document `APP_FLAGS` somehow...
    * Exit code with no flags is -2 from `meow`, just needs to be noted
    * Document `config.destinations` is space sep string or array (`NormalizeToArrayIfPossible`)
    * Document ComplexSearch structure and flexibility!!!!!
        * Regex is dope, but escapes need double escapes in this context if using single quotes
            * JSON should be fine...
    * Fully document descriptor configs
        * Header name suggestion: "Descriptor Config: Sources"
    * Fully document jobs and all their flags!
    * Try not to use `.*` in regex patterns!!!!
        * Really slow, use sparingly
* Build a Test Source/Dest for examples/testing out of the box
* Build a "acceptall" predicate string handler to support accepting EVERYTHING in a source!
* Build a `disabled` semaphore in Dest that still marks as processed. This avoids spam but still keeps stats.
    * Useful for seasonal style notifications.
        * ie, as stats start showing an increase in catches, you can then re-enable
* Build a `extraOptions.headers` templating json object something-or-other that will overlay on the request
    * This should easily allow/support API requests
* Build a `json` source
    * Should also utilize the `extraOptions.headers` templating construct/concept
* MUST FIX:
    * DestinationIDs should actually only be 1 per Entry
        * Essentially, need to create an Entry *per* destination. This is so each destination has it's own error and isNotified statuses. Without this, as is, one out of 5 (or however many) destinations may pass and it will mark as if ALL were isNotified - which simply isn't true!
    * Need to figure out why docker volume mount is owned by root sometimes... Can ruin a dev's env that doesn't have root access
* Review all logging output
    * normalize colorization
    * normalize labeling (ie, [descriptor.id] prefixes, vs suffix, etc)

* Find more sources for deals:
    * Twitter Searches? Do they go to RSS?

### Nice to haves

* Source: Caching recent requests to avoid hyper-hitting servers (aka same feed but differet predicates)
* Source: Native JSON source? (Reddit supports JSON via `.json` instead of `.rss`)
* Destination: Implement Twitter Posting
* Source: Implement Web Scraping? Maybe super low priority?
