const TABLE_NAME = 'entries';

exports.up = function (knex) {
  return knex.schema.alterTable(TABLE_NAME, table => {
    table.boolean('isDeleted').defaultTo(false);
  });
};

exports.down = function (knex) {
  return knex.schema.alterTable(TABLE_NAME, table => {
    table.dropColumn('isDeleted');
  });
};
